const mensaje = {};

mensaje.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM mensaje', (err, mensajes) => {
            if (err) {
                res.json(err);
            }
            res.json(mensajes)
        });
    });
};

mensaje.save = (req, res) => {
    const data = req.body;

    req.getConnection((err, connection) => {
        const query = connection.query('INSERT INTO mensaje set ?', data, (err, mensaje) => {
            if (err) {
                res.json({ respuesta: "Error al insertar" });
            }
            res.json({ respuesta: "Se ha enviado tu mensaje" });
        })
    })

};

module.exports = mensaje;