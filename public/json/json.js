const bd = [
    {"id":0, "Descripcion":"Sombrero de paja toquilla clasico con erreglo en color negro. producto1"},
    {"id":1, "Descripcion":"Sombrero de paja toquilla clasico con erreglo en color negro. producto2"},
    {"id":2, "Descripcion":"Sombrero de paja toquilla clasico con erreglo en color negro. producto3"},
    {"id":3, "Descripcion":"Sombrero de paja toquilla clasico con erreglo en color negro. producto4"},
    {"id":4, "Descripcion":"Sombrero de paja toquilla clasico con erreglo en color negro. producto5"},
    {"id":5, "Descripcion":"Sombrero de paja toquilla clasico con erreglo en color negro. producto6"},

]

const product = document.querySelectorAll('.des_product');

product.forEach((produc)=>{
    produc.addEventListener('click', (descripcion)=>{
        let id=descripcion.target.getAttribute('producto-id');
        bd.forEach((produc)=>{
            if(id == produc.id){
                const verDetalle = descripcion.target.parentElement.lastElementChild;
                verDetalle.innerHTML =`
                    <div class="lista"
                        <div class="nom">
                            <br>
                            <p>${produc.Descripcion}</p>
                        </div>
                    </div>`
            }
        })
    })
})