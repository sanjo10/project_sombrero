const usuario = {};

usuario.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM usuario', (err, usuarios) => {
            if (err) {
                res.json(err);
            }
            res.json(usuarios)
        });
    });
};

usuario.save = (req, res) => {
    let error = "";
    const data = req.body;
    if (data.nombre.length < 2) {
        error += "\nIngrese un nombre valido "
    }
    if (data.apellido.length < 2) {
        error += "\nIngrese un apellido valido "
    }
    if (data.correo.length < 5) {
        error += "\nIngrese un correo valido "
    }
    if (data.password.length < 8) {
        error += "\nLa contraseña debe tener al menos 8 digitos "
    }
    if (error.length > 1) {
        res.render("succes", {
            data: error
        });
    }
    else {
        req.getConnection((err, connection) => {
            const query = connection.query('INSERT INTO usuario set ?', data, (err, usuario) => {
                if (err) {
                    res.json({ respuesta: "Error al insertar" });
                }
                res.render("succes", {
                    data: true
                });
            })
        })
    }
};

usuario.getOne = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM usuario WHERE id = ?", [id], (err, rows) => {
            res.json(rows[0])
        });
    });
};

usuario.update = (req, res) => {
    const { id } = req.params;
    const newUsuario = req.body;
    req.getConnection((err, conn) => {
        conn.query('UPDATE usuario set ? where id = ?', [newUsuario, id], (err, rows) => {
            if (err) {
                res.json({ respuesta: "Error al actualizar" });
            }
            res.json({ respuesta: "Actualizado satisfactoriamente" })
        });
    });
};

usuario.delete = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, connection) => {
        connection.query('DELETE FROM usuario WHERE id = ?', [id], (err, rows) => {
            if (err) {
                res.json({ respuesta: "Error al Eliminar" });
            }
            res.json({ respuesta: "Eliminado satisfactoriamente" })
        });
    });
}

module.exports = usuario;