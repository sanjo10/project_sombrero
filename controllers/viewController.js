const view = {};

view.index=(req, res) => {
    res.render('index');
}

view.conoce=(req, res) => {
    res.render('conoce');
}

view.contacto=(req, res) => {
    res.render('contacto');
}

view.login=(req, res) => {
    res.render('login');
}

view.productos=(req, res) => {
    res.render('productos');
}

module.exports = view;