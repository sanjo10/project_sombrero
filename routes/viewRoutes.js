const router = require('express').Router();

const viewController = require('../controllers/viewController');

router.get('/', viewController.index);
router.get('/conoce', viewController.conoce);
router.get('/contacto', viewController.contacto);
router.get('/login', viewController.login);
router.get('/productos', viewController.productos);


module.exports = router;