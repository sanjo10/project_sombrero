const express = require('express'),
      path = require('path'),
      morgan = require('morgan'),
      mysql = require('mysql'),
      myConnection = require('express-myconnection');

const app = express();

// configuraciones
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// middlewares
app.use(morgan('dev'));

//conexion a la BD
app.use(myConnection(mysql, {
  host: 'localhost',
  user: 'root',
  password: '',
  port: 3306,
  database: 'sombreros'
}, 'single'));
app.use(express.urlencoded({extended: false}));

// importando routes
const productoRoutes = require('./routes/productoRoute');
const usuarioRoutes = require ('./routes/usuarioRoutes');
const mensajeRoutes = require ('./routes/mensajeRoutes');
const viewRoutes = require ('./routes/viewRoutes');

// Usando routes
app.use('/producto', productoRoutes);
app.use('/usuario', usuarioRoutes);
app.use('/mensaje', usuarioRoutes);

app.use(viewRoutes);

// static files
app.use(express.static(path.join(__dirname, 'public')));

// starting the server
app.listen(app.get('port'), () => {
  console.log(`server on port ${app.get('port')}`);
});