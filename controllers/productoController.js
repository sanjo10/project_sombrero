const producto = {};

producto.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM producto', (err, productos) => {
            if (err) {
                res.json(err);
            }
            res.json(productos)
        });
    });
};

producto.save = (req, res) => {
    const data = req.body;
    req.getConnection((err, connection) => {
        const query = connection.query('INSERT INTO producto set ?', data, (err, producto) => {
            if (err) {
                res.json({ respuesta: "Error al insertar" });
            }
            res.json({respues:"Se ha ingresado correctamente"});
        })
    })
};

producto.getOne = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM producto WHERE id = ?", [id], (err, rows) => {
            res.json(rows[0])
        });
    });
};

producto.update = (req, res) => {
    const { id } = req.params;
    const newproducto = req.body;
    req.getConnection((err, conn) => {
        conn.query('UPDATE producto set ? where id = ?', [newproducto, id], (err, rows) => {
            if (err) {
                res.json({ respuesta: "Error al actualizar" });
            }
            res.json({ respuesta: "Actualizado satisfactoriamente" })
        });
    });
};

producto.delete = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, connection) => {
        connection.query('DELETE FROM producto WHERE id = ?', [id], (err, rows) => {
            if (err) {
                res.json({ respuesta: "Error al Eliminar" });
            }
            res.json({ respuesta: "Eliminado satisfactoriamente" })
        });
    });
}

module.exports = producto;