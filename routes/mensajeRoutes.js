const router = require('express').Router();

const mensajeController = require('../controllers/mensajes.Controller');

router.get('/', mensajeController.list);
router.post('/add', mensajeController.save);

module.exports = router;